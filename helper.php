<?php

/**
 * Helper class for mod_businesshour
 *
 * @package         Joomla!-Module Businesshour for Joomla! 3.x
 * @author          Michael S. RitZenhoff
 * @author url      https://webmanagement.berlin
 * @author email    contact@webmanagement.berlin
 * @copyright       Copyright (C) 2015. Michael S. RitZenhoff. All rights reserved.
 * @license         GNU/GPLv3, http://www.gnu.org/licenses/gpl-3.0.html, see /assets/en-GB.license.txt
 * */
// no direct access
defined('_JEXEC') or die('Restricted access');

/* ========================================================================= */

class modBusinesshourHelper {

  public static function getBusinesshour($params) {
    // https://docs.joomla.org/Securing_Joomla_extensions

    $database = JFactory::getDbo();
    $string = $database->escape($string);

    $value = intval($_GET['value']);

    $query = $database->getQuery(true);
    $query = "SELECT * FROM #__modules mod_businesshour WHERE id = $value";

    $database->setQuery($query);

    $result = $database->loadResult();

    return $result;
  }

}
