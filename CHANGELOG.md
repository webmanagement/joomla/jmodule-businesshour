## Changes to mod_businesshour

#### Version 0.1.6 05.04.2021
* Extended the table for times at afternoon

#### Version 0.1.4 20.12.2017
* removed static protected querys in helper.php

#### Version 0.1.3 11.10.2017
* Multiple Bug Fix

#### Version 0.1.2 10.10.2017
* Multiple Bug Fix

#### Version 0.1.0 01.09.2017
* First lokal Installation of Businesshour Module
