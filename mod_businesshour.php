<?php

/**
 * mod_businesshour Module Entry Point
 *
 * @package         Joomla!-Module Businesshour for Joomla! 3.x
 * @author          Michael S. RitZenhoff
 * @author url      https://webmanagement.berlin
 * @author email    contact@webmanagement.berlin
 * @copyright       Copyright (C) 2015. Michael S. RitZenhoff. All rights reserved.
 * @license         GNU/GPLv3, http://www.gnu.org/licenses/gpl-3.0.html, see /assets/en-GB.license.txt
 * */
// no direct access
defined('_JEXEC') or die('Restricted access');

$document = JFactory::getDocument();

$base = JUri::base(true);
$modTemplate = $base . "modules/{$module->module}/tmpl/";

//include helpers of the component and module
require_once dirname(__FILE__) . '/helper.php';

$businesshour = modBusinesshourHelper::getBusinesshour($params);

/* # *
 * ========================================================================= */

$showtitle = htmlspecialchars($module->showtitle);
$title = htmlspecialchars($module->title);

/* # *
 * ========================================================================= */

// add CSS
$businesshourcss = htmlspecialchars($params->get('businesshourcss'));
if ($businesshourcss == 1) :
  JFactory::getDocument()->addStyleSheet($modTemplate . 'css/businesshour.min.css');
endif;

/* # *
 * ========================================================================= */

// Label Tab
/**
$monday = htmlspecialchars($params->get('monday'));
$tuesday = htmlspecialchars($params->get('tuesday'));
$wednesday = htmlspecialchars($params->get('wednesday'));
$thursday = htmlspecialchars($params->get('thursday'));
$friday = htmlspecialchars($params->get('friday'));
$saturday = htmlspecialchars($params->get('saturday'));
$sunday = htmlspecialchars($params->get('sunday'));
**/
$businesshourlocationname = htmlspecialchars($params->get('businesshourlocationname'));
$businesshourlocationdesc = htmlspecialchars($params->get('businesshourlocationdesc'));
$ohFAwesome = htmlspecialchars($params->get('ohFAwesome'));


// OpenHours Tab
$openhoursname = htmlspecialchars($params->get('openhoursnname'));
$openhoursdesc = htmlspecialchars($params->get('openhoursdesc'));

 // Week closed
$ohWeekClosed = htmlspecialchars($params->get('ohWeekClosed'));
$ohWeekClosedMessage = htmlspecialchars($params->get('ohWeekClosedMessage'));

// Day closed
$ohDayClosedMessage = htmlspecialchars($params->get('ohDayClosedMessage'));

// Daily Split Time 
$morning = htmlspecialchars($params->get('morning'));
$afternoon = htmlspecialchars($params->get('afternoon'));

// Monday Time Tab
$ohMonday = htmlspecialchars($params->get('ohMonday'));
$ohMondayShow = htmlspecialchars($params->get('ohMondayShow'));
$ohMonday_1 = htmlspecialchars($params->get('ohMonday_1'));
$ohMonday_2 = htmlspecialchars($params->get('ohMonday_2'));
$ohMonday_3 = htmlspecialchars($params->get('ohMonday_3'));
$ohMonday_4 = htmlspecialchars($params->get('ohMonday_4'));

// Tuesday Time Tab
$ohTuesday = htmlspecialchars($params->get('ohTuesday'));
$ohTuesdayShow = htmlspecialchars($params->get('ohTuesdayShow'));
$ohTuesday_1 = htmlspecialchars($params->get('ohTuesday_1'));
$ohTuesday_2 = htmlspecialchars($params->get('ohTuesday_2'));
$ohTuesday_3 = htmlspecialchars($params->get('ohTuesday_3'));
$ohTuesday_4 = htmlspecialchars($params->get('ohTuesday_4'));

// Wednesday Time Tab
$ohWednesday = htmlspecialchars($params->get('ohWednesday'));
$ohWednesdayShow = htmlspecialchars($params->get('ohWednesdayShow'));
$ohWednesday_1 = htmlspecialchars($params->get('ohWednesday_1'));
$ohWednesday_2 = htmlspecialchars($params->get('ohWednesday_2'));
$ohWednesday_3 = htmlspecialchars($params->get('ohWednesday_3'));
$ohWednesday_4 = htmlspecialchars($params->get('ohWednesday_4'));

// Thursday Time Tab
$ohThursday = htmlspecialchars($params->get('ohThursday'));
$ohThursdayShow = htmlspecialchars($params->get('ohThursdayShow'));
$ohThursday_1 = htmlspecialchars($params->get('ohThursday_1'));
$ohThursday_2 = htmlspecialchars($params->get('ohThursday_2'));
$ohThursday_3 = htmlspecialchars($params->get('ohThursday_3'));
$ohThursday_4 = htmlspecialchars($params->get('ohThursday_4'));

// Friday Time Tab
$ohFriday = htmlspecialchars($params->get('ohFriday'));
$ohFridayShow = htmlspecialchars($params->get('ohFridayShow'));
$ohFriday_1 = htmlspecialchars($params->get('ohFriday_1'));
$ohFriday_2 = htmlspecialchars($params->get('ohFriday_2'));
$ohFriday_3 = htmlspecialchars($params->get('ohFriday_3'));
$ohFriday_4 = htmlspecialchars($params->get('ohFriday_4'));

// Saturday Time Tab
$ohSaturday = htmlspecialchars($params->get('ohSaturday'));
$ohSaturdayShow = htmlspecialchars($params->get('ohSaturdayShow'));
$ohSaturday_1 = htmlspecialchars($params->get('ohSaturday_1'));
$ohSaturday_2 = htmlspecialchars($params->get('ohSaturday_2'));
$ohSaturday_3 = htmlspecialchars($params->get('ohSaturday_3'));
$ohSaturday_4 = htmlspecialchars($params->get('ohSaturday_4'));

// Sunday Time Tab
$ohSunday = htmlspecialchars($params->get('ohSunday'));
$ohSundayShow = htmlspecialchars($params->get('ohSundayShow'));
$ohSunday_1 = htmlspecialchars($params->get('ohSunday_1'));
$ohSunday_2 = htmlspecialchars($params->get('ohSunday_2'));
$ohSunday_3 = htmlspecialchars($params->get('ohSunday_3'));
$ohSunday_4 = htmlspecialchars($params->get('ohSunday_4'));


// Location Tab
$street = htmlspecialchars($params->get('street'));
$extendedaddress = htmlspecialchars($params->get('extendedaddress'));
$postalcode = htmlspecialchars($params->get('postalcode'));
$city = htmlspecialchars($params->get('city'));
$region = htmlspecialchars($params->get('region'));
$country = htmlspecialchars($params->get('country'));

$georegion = htmlspecialchars($params->get('georegion'));
$geoplacename = htmlspecialchars($params->get('geoplacename'));
$geolatitude = htmlspecialchars($params->get('geolatitude'));
$geolongitude = htmlspecialchars($params->get('geolongitude'));
$geoaltitude = htmlspecialchars($params->get('geoaltitude'));
$geohidden = htmlspecialchars($params->get('geohidden'));


// Contact Tab
$telephone = htmlspecialchars($params->get('telephone'));
$telephonecall = htmlspecialchars($params->get('telephonecall'));

$telefax = htmlspecialchars($params->get('telefax'));

$mobilephone = htmlspecialchars($params->get('mobilephone'));
$mobilephonecall = htmlspecialchars($params->get('mobilephonecall'));
$mobilephonesms = htmlspecialchars($params->get('mobilephonesms'));

// Additional Tab
$accessory = $params->get('accessory');

// Extras Tab
$businesshourfooter = htmlspecialchars($params->get('businesshourfooter'));
$businesshourcopy = htmlspecialchars($params->get('businesshourcopy'));
$microdataopenhour = htmlspecialchars($params->get('microdataopenhour'));

/* # *
 * ========================================================================= */

$space = '&nbsp;';
$hidden = 'hidden';

$businesshourkey = '';

// LINKS
$businesshourhome = '<a href="https://gitlab.com/webmanagement/joomla/jmodule-businesshour" title="Businesshour &ndash; Module for Joomla! 3.x & 4.x">Businesshour</a>';
$microdataopenhour = 'This content uses <a target="_blank" href="//schema.org/">schema.org</a>';

/* # *
 * ========================================================================= */

$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));

// Render the module
require JModuleHelper::getLayoutPath('mod_businesshour', $params->get('layout', 'default'));
