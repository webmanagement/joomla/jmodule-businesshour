<?php
/**
 * default for mod_businesshour
 *
 * @package         Joomla!-Module Businesshour for Joomla! 3.x
 * @author          Michael S. RitZenhoff
 * @author url      https://webmanagement.berlin
 * @author email    contact@webmanagement.berlin
 * @copyright       Copyright (C) 2015. Michael S. RitZenhoff. All rights reserved.
 * @license         GNU/GPLv3, http://www.gnu.org/licenses/gpl-3.0.html, see /assets/en-GB.license.txt
 * */
// No direct access
defined('_JEXEC') or die('Restricted access');

/*
 * <article <?php $moduleclass_sfx ? print 'class="' . $moduleclass_sfx . '"' : ''; ?>>
 */
if ($showtitle == 1) :
  ?>
  <header>
    <h2 role="heading"><?php print $title; ?></h2>
  </header>
  <?php
endif;
?>
<section data-role="businesshour">
  <h3 hidden><?php print 'Businesshour for ' . $businesshourlocationname; ?></h3>

  <?php
  /** # **/
  /**
  ** http://linter.structured-data.org/examples/schema.org/OpeningHoursSpecification/
  ** https://stackoverflow.com/questions/17058982/different-type-of-openinghours-per-day-using-schema-org
  **/
  ?>
  <div itemscope itemtype="http://schema.org/Place">
    <link itemprop="http://www.w3.org/1999/02/22-rdf-syntax-ns#type" href="http://purl.org/goodrelations/v1#Location" />

    <?php
    if ($businesshourlocationname !== 0):
      print '<h3 itemprop="name">' . $businesshourlocationname . '</h3>';
    endif;
    if ($businesshourlocationdesc !== 0):
      print '<span itemprop="description">' . $businesshourlocationdesc . '</span>';
    endif;
    ?>

    <div itemprop=“openingHoursSpecification“ itemscope itemtype=“http://schema.org/OpeningHoursSpecification“>
      <?php
      require_once dirname(__FILE__) . '/openhour-table.php';
      ?>
    </div>

  <?php
  /*
   * $organizationname
   */
  if ($organizationname) :
    ?>
    <div class="h-card vcard" typeof="Organization" itemscope itemtype="http://schema.org/Organization">
      <?php
    else:
      ?>
      <div class="h-card vcard">
      <?php
      endif;
      ?>

      <?php
      /*
       * $organizationname
       * $organizationdesc
       */
      if ($organizationname) :
        ?>
        <span class="p-org org">
          <?php
          print $organizationname ? '<span class="p-organization-name organization-name" itemprop="legalName">' . $organizationname . '</span>' : '';
          print $organizationdesc ? '<span class="p-category category" itemprop="description">' . $organizationdesc . '</span>' : '';
          ?>
        </span>
        <?php
      endif;
      ?>

      <?php
      /*
       * $street
       * $extendedaddress
       * $postalcode
       * $city
       * $region
       * $country
       */
      ?>
      <span itemprop="location" itemscope itemtype="http://schema.org/Place">
        <?php
        if ($postofficeboxaddress || $street || $postalcode || $city) :
          ?>
          <span class="p-adr h-adr adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
            <?php
            print $street ? '<span class="p-street-address street-address" itemprop="streetAddress">' . $street . '</span>' : '';

            print $extendedaddress ? '<span class="p-extended-address extended-address">' . $extendedaddress . '</span>' : '';

            if ($postalcode || $city) :
              ?>
              <span class="city">
                <?php
                print $postalcode ? '<span class="p-postal-code postal-code" itemprop="postalCode">' . $postalcode . $space . '</span>' : '';
                print $city ? '<span class="p-locality locality" itemprop="addressLocality">' . $city . '</span>' : '';
                ?>
              </span>
              <?php
            endif;
            ?>
          </span>
          <?php
        endif;
        ?>

        <?php
        /*
         * $geohidden
         *
         * $geolatitude
         * $geolongitude
         * $geoaltitude
         */
        if ($geolatitude && $geolongitude) :
          ?>
          <span class="h-geo" itemprop="geo" itemscope itemtype="http://schema.org/GeoCoordinates"<?php print ($geohidden == 1) ? ' ' . $hidden : ''; ?>>
            <span class="type work">GEO:<?php print $space; ?></span>
            <?php // backward compatibility  ?>
            <span class="p-latitude" itemprop="latitude"><?php print $geolatitude; ?></span>
            <span class="p-longitude" itemprop="longitude"><?php print ';' . $geolongitude; ?></span>
            <?php
            if ($geoaltitude) :
              ?>
              <span class="p-altitude" itemprop="altitude"><?php print ';' . $geoaltitude; ?></span>
              <?php
            endif;
            ?>
          </span>
          <?php
        endif;
        ?>

        <?php
        /*
         * $telephone
         * $telephonephonecall
         */
        if ($telephone) :
          ?>
          <span class="p-tel cell" itemprop="telephone">
            <span class="type work VOICE">Tel:<?php print $space; ?></span>
            <?php
            if($telephonecall == 1):
              print '<a class="u-url" href="tel:' . $telephone . '" title="Anruf t&auml;tigen"><span></span>' . $telephone . '</a>';
            else:
              print '<span class="value" property="telephone">'. $telephone . '</span>';
            endif;
            ?>
          </span>
          <?php
        endif;

        /*
         * $telefax
         */
        if ($telefax) :
          ?>
          <span class="p-fax fax" itemprop="faxNumber">
            <span class="type fax work">Fax:<?php print $space; ?></span>
            <span class="value"><?php print $telefax; ?></span>
          </span>
          <?php
        endif;

        /*
         * $mobilephone
         * $mobilephonecall
         * $mobilephonesms
         */
        if ($mobilephone) :
          ?>
          <span class="p-tel mobil" itemprop="telephone">
            <span class="type work VOICE mobil msg">Mobil:<?php print $space; ?></span>
            <?php
            if($mobilephonecall == 1):
              print '<a class="u-url" href="tel:' . $mobilephone . '" title="Anruf t&auml;tigen"><span></span>' . $mobilephone . '</a>';
            else:
              print '<span class="value" property="telephone">'. $mobilephone . '</span>';
            endif;
            ?>
          </span>
          <?php
        endif;
        ?>
      </span>
    </div>

  </div>

</section>

<section>
  <h3 hidden><?php print 'Extended Information for ' . $businesshourlocationname; ?></h3>
  <?php
  /*
   * $businesshourfooter
   * $businesshourcopy
   * $microdataopenhour
   */
  if ((empty($businesshourkey) == ($businesshourkey == 0)) && $microdataopenhour == 0 || (empty($businesshourkey) == ($businesshourkey == 0)) && $microdataopenhour == 1):
    print '<div class="businesshourfooter">';
    print ($businesshourkey == 0) ? '<span class="businesshourcopy">' . (date('Y') == '2015') ? '&#169; ' . htmlspecialchars(date('Y')) . $space . $businesshourhome : '&#169; 2014&ndash;' . htmlspecialchars(date('Y')) . $businesshourhome . '</span>' : '';
    print ($microdataopenhour == 1) ? '<span class="microdataopenhour">' . $microdataopenhour . '</span>' : '';
    print '</div>';
  endif;
  ?>
</section>

<?php
/*
 * $accessory
 */
if ($accessory):
  ?>
  <section>
    <hr>
    <h3 hidden><?php print 'Additional Information for ' . $businesshourlocationname; ?></h3>
    <?php
    if ($accessory):
      print $accessory;
    endif;
    ?>
  </section>
  <?php
endif;

/*
 * </article>
 */