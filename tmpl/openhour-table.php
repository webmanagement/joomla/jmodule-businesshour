<table style="width: 100%;">
  <tbody>
  <?php

  $toDay = date("D");
  //print '' . $toDay . '';

  if ($toDay == 'Mon'):
    $toDayMon = ' class="toDay-Mon"';
  elseif ($toDay == 'Tue'):
    $toDayTue = ' class="toDay-Tue"';
  elseif ($toDay == 'Wed'):
    $toDayWed = ' class="toDay-Wed"';
  elseif ($toDay == 'Thu'):
    $toDayThu = ' class="toDay-Thu"';
  elseif ($toDay == 'Fri'):
    $toDayFri = ' class="toDay-Fri"';
  elseif ($toDay == 'Sat'):
    $toDaySat = ' class="toDay-Sat"';
  elseif ($toDay == 'Sun'):
    $toDaySun = ' class="toDay-Sun"';
  endif;

  /** ** /
  $monday = 'Monday';
  $tuesday = 'Tuesday';
  $wednesday = 'Wednesday';
  $thursday = 'Thursday';
  $friday = 'Friday';
  $saturday = 'Saturday';
  $sunday = 'Sunday';
  $morning = 'Morning';
  $afternoon = 'Afternoon'
  **/
  $monday = 'Montag';
  $tuesday = 'Dienstag';
  $wednesday = 'Mittwoch';
  $thursday = 'Donnerstag';
  $friday = 'Freitag';
  $saturday = 'Samstag';
  $sunday = 'Sonntag';
  $morning = 'Vormittag';
  $afternoon = 'Nachmittag';

  if($fontAwesomeClock == 1):
    $faClock = '<span class="fa fa-clock-o"></span> ';
  endif;

  $start_tr    = '<tr>';
  $dayOfWeek   = '<link itemprop="hasOpeningHoursDayOfWeek" href="http://schema.org';
  $toDayClosed = '<td class="toDayClosed" colspan="3">';
  $opens       = 'itemprop="opens"';
  $closes      = 'itemprop="closes"';
  $end_tr      = '</tr>';

  /** START show one row for closed if is holiday or something **/
  if ($ohWeekClosed == 1):

    print $start_tr;
      print '<td class="toDayClosed">' . $ohWeekClosedMessage . '</td>';
    print $end_tr;

  else:

    /* Monday */
    if ($ohMondayShow == 1):

      print $start_tr;
        print '<td' . $toDayMon . '>' . $dayOfWeek . '/Monday">' . $faClock . $monday . '</td>';
        if( $ohMonday_1 == 0 && $ohMonday_2 == 0 ):
          print $toDayClosed . $ohDayClosedMessage . '</td>';
        else:
          $ohMonday_1 ? print '<td' . $toDayMon . '><time ' . $opens . ' content="' . JText::_( $ohMonday_1 ) . '">' . JText::_( $ohMonday_1 ) . '</time></td>' : '<td></td>';
          print '<td' . $toDayMon . '>&#8211;</td>';
          $ohMonday_2 ? print '<td' . $toDayMon . '><time ' . $closes . ' content="' . JText::_( $ohMonday_2 ) . '">' . JText::_( $ohMonday_2 ) . '</time></td>' : '<td></td>';
        endif;
      print $end_tr;

      if( $ohMonday_3 == 0 && $ohMonday_4 == 0 ):
        print '';
      else:
        print $start_tr;
        print '<td></td>';
        $ohMonday_3 ? print '<td' . $toDayMon . '><time ' . $opens . ' content="' . JText::_( $ohMonday_3 ) . '">' . JText::_( $ohMonday_3 ) . '</time></td>' : '<td></td>';
        print '<td' . $toDayMon . '>&#8211;</td>';
        $ohMonday_4 ? print '<td' . $toDayMon . '><time ' . $closes . ' content="' . JText::_( $ohMonday_4 ) . '">' . JText::_( $ohMonday_4 ) . '</time></td>' : '<td></td>';
        print $end_tr;
      endif;

    else:
      print '';
    endif;

    /* Tuesday */
    if ($ohTuesdayShow == 1):

      print $start_tr;
        print '<td' . $toDayTue . '>' . $dayOfWeek . '/Tuesday">' . $faClock . $tuesday . '</td>';
        if( $ohTuesday_1 == 0 && $ohTuesday_2 == 0 ):
          print $toDayClosed . $ohDayClosedMessage . '</td>';
        else:
          $ohTuesday_1 ? print '<td' . $toDayTue . '><time ' . $opens . ' content="' . $ohTuesday_1 . '">' . $ohTuesday_1 . '</time></td>' : '<td></td>';
          print '<td' . $toDayTue . '>&#8211;</td>';
          $ohTuesday_2 ? print '<td' . $toDayTue . '><time ' . $closes . ' content="' . $ohTuesday_2 . '">' . $ohTuesday_2 . '</time></td>' : '<td></td>';
        endif;
      print $end_tr;

      if( $ohTuesday_3 == 0 && $ohTuesday_4 == 0 ):
        print '';
      else:
        print $start_tr;
        print '<td></td>';
        $ohTuesday_3 ? print '<td' . $toDayTue . '><time ' . $opens . ' content="' . $ohTuesday_3 . '">' . $ohTuesday_3 . '</time></td>' : '<td></td>';
        print '<td' . $toDayTue . '>&#8211;</td>';
        $ohTuesday_4 ? print '<td' . $toDayTue . '><time ' . $closes . ' content="' . $ohTuesday_4 . '">' . $ohTuesday_4 . '</time></td>' : '<td></td>';
        print $end_tr;
      endif;

    else:
      print '';
    endif;

    /* Wednesday */
    if ($ohWednesdayShow == 1):

      print $start_tr;
        print '<td' . $toDayWed . '>' . $dayOfWeek . '/Wednesday">' . $faClock . $wednesday . '</td>';
        if( $ohWednesday_1 == 0 && $ohWednesday_2 == 0 ):
          print $toDayClosed . $ohDayClosedMessage . '</td>';
        else:
          $ohWednesday_1 ? print '<td' . $toDayWed . '><time ' . $opens . ' content="' . $ohWednesday_1 . '">' . $ohWednesday_1 . '</time></td>' : '<td></td>';
          print '<td' . $toDayWed . '>&#8211;</td>';
          $ohWednesday_2 ? print '<td' . $toDayWed . '><time ' . $closes . ' content="' . $ohWednesday_2 . '">' . $ohWednesday_2 . '</time></td>' : '<td></td>';
        endif;
      print $end_tr;

      if( $ohWednesday_3 == 0 && $ohWednesday_4 == 0 ):
        print '';
      else:
        print $start_tr;
        print '<td></td>';
        $ohWednesday_3 ? print '<td' . $toDayWed . '><time ' . $opens . ' content="' . $ohWednesday_3 . '">' . $ohWednesday_3 . '</time></td>' : '<td></td>';
        print '<td' . $toDayWed . '>&#8211;</td>';
        $ohWednesday_4 ? print '<td' . $toDayWed . '><time ' . $closes . ' content="' . $ohWednesday_4 . '">' . $ohWednesday_4 . '</time></td>' : '<td></td>';
        print $end_tr;
      endif;

    else:
      print '';
    endif;

    /* Thursday */
    if ($ohThursdayShow == 1):

      print $start_tr;
        print '<td' . $toDayThu . '>' . $dayOfWeek . '/Thursday">' . $faClock . $thursday . '</td>';
        if( $ohThursday_1 == 0 && $ohThursday_2 == 0 ):
          print $toDayClosed . $ohDayClosedMessage . '</td>';
        else:
          $ohThursday_1 ? print '<td' . $toDayThu . '><time ' . $opens . ' content="' . $ohThursday_1 . '">' . $ohThursday_1 . '</time></td>' : '<td></td>';
          print '<td' . $toDayThu . '>&#8211;</td>';
          $ohThursday_2 ? print '<td' . $toDayThu . '><time ' . $closes . ' content="' . $ohThursday_2 . '">' . $ohThursday_2 . '</time></td>' : '<td></td>';
        endif;
      print $end_tr;

      if( $ohThursday_3 == 0 && $ohThursday_4 == 0 ):
        print '';
      else:
        print $start_tr;
        print '<td></td>';
        $ohThursday_3 ? print '<td' . $toDayThu . '><time ' . $opens . ' content="' . $ohThursday_3 . '">' . $ohThursday_3 . '</time></td>' : '<td></td>';
        print '<td' . $toDayThu . '>&#8211;</td>';
        $ohThursday_4 ? print '<td' . $toDayThu . '><time ' . $closes . ' content="' . $ohThursday_4 . '">' . $ohThursday_4 . '</time></td>' : '<td></td>';
        print $end_tr;
      endif;

    else:
      print '';
    endif;

    /* Friday */
    if ($ohFridayShow == 1):

      print $start_tr;
        print '<td' . $toDayFri . '>' . $dayOfWeek . '/Friday">' . $faClock . $friday . '</td>';
        if( $ohFriday_1 == 0 && $ohFriday_2 == 0 ):
          print $toDayClosed . $ohDayClosedMessage . '</td>';
        else:
          $ohFriday_1 ? print '<td' . $toDayFri . '><time ' . $opens . ' content="' . $ohFriday_1 . '">' . $ohFriday_1 . '</time></td>' : '<td></td>';
          print '<td' . $toDayFri . '>&#8211;</td>';
          $ohFriday_2 ? print '<td' . $toDayFri . '><time ' . $closes . ' content="' . $ohFriday_2 . '">' . $ohFriday_2 . '</time></td>' : '<td></td>';
        endif;
      print $end_tr;

      if( $ohFriday_3 == 0 && $ohSaturday_4 == 0 ):
        print '';
      else:
        print $start_tr;
        print '<td></td>';
        $ohFriday_3 ? print '<td' . $toDayFri . '><time ' . $opens . ' content="' . $ohFriday_3 . '">' . $ohFriday_3 . '</time></td>' : '<td></td>';
        print '<td' . $toDayFri . '>&#8211;</td>';
        $ohFriday_4 ? print '<td' . $toDayFri . '><time ' . $closes . ' content="' . $ohFriday_4 . '">' . $ohFriday_4 . '</time></td>' : '<td></td>';
        print $end_tr;
      endif;

    else:
      print '';
    endif;

    /* Saturday */
    if ($ohSaturdayShow == 1):

      print $start_tr;
        print '<td' . $toDaySat . '>' . $dayOfWeek . '/Saturday">' . $faClock . $saturday . '</td>';
        if( $ohSaturday_1 == 0 && $ohSaturday_2 == 0 ):
          print $toDayClosed . $ohDayClosedMessage . '</td>';
        else:
          $ohSaturday_1 ? print '<td' . $toDaySat . '><time ' . $opens . ' content="' . $ohSaturday_1 . '">' . $ohSaturday_1 . '</time></td>' : '<td></td>';
          print '<td' . $toDaySat . '>&#8211;</td>';
          $ohSaturday_2 ? print '<td' . $toDaySat . '><time ' . $closes . ' content="' . $ohSaturday_2 . '">' . $ohSaturday_2 . '</time></td>' : '<td></td>';
        endif;
      print $end_tr;

      if( $ohSaturday_3 == 0 && $ohSaturday_4 == 0 ):
        print '';
      else:
        print $start_tr;
        print '<td></td>';
        $ohSaturday_3 ? print '<td' . $toDaySat . '><time ' . $opens . ' content="' . $ohSaturday_3 . '">' . $ohSaturday_3 . '</time></td>' : '<td></td>';
        print '<td' . $toDaySat . '>&#8211;</td>';
        $ohSaturday_4 ? print '<td' . $toDaySat . '><time ' . $closes . ' content="' . $ohSaturday_4 . '">' . $ohSaturday_4 . '</time></td>' : '<td></td>';
        print $end_tr;
      endif;

    else:
      print '';
    endif;

    /* Sunday */
    if ($ohSundayShow == 1):

      print $start_tr;
        print '<td' . $toDaySun . '>' . $dayOfWeek . '/Sunday">' . $faClock . $sunday . '</td>';
        if( $ohSunday_1 == 0 && $ohSunday_2 == 0 ):
          print $toDayClosed . $ohDayClosedMessage . '</td>';
        else:
          $ohSunday_1 ? print '<td' . $toDaySun . '><time ' . $opens . ' content="' . $ohSunday_1 . '">' . $ohSunday_1 . '</time></td>' : '<td></td>';
          print '<td' . $toDaySun . '>&#8211;</td>';
          $ohSunday_2 ? print '<td' . $toDaySun . '><time ' . $closes . ' content="' . $ohSunday_2 . '">' . $ohSunday_2 . '</time></td>' : '<td></td>';
        endif;
      print $end_tr;

      if( $ohSunday_3 == 0 && $ohSunday_4 == 0 ):
        print '';
      else:
        print $start_tr;
        print '<td></td>';
        $ohSunday_3 ? print '<td' . $toDaySun . '><time ' . $opens . ' content="' . $ohSunday_3 . '">' . $ohSunday_3 . '</time></td>' : '<td></td>';
        print '<td' . $toDaySun . '>&#8211;</td>';
        $ohSunday_4 ? print '<td' . $toDaySun . '><time ' . $closes . ' content="' . $ohSunday_4 . '">' . $ohSunday_4 . '</time></td>' : '<td></td>';
        print $end_tr;
      endif;

    else:
      print '';
    endif;

  /** END show one row for closed if is holiday or something **/
  endif;
  ?>
  </tbody>
</table>
