# jModule Businesshour
Version 0.1.6

### Businesshour Module for Joomla! 3.x & 4.x


**Businesshour to present open hours on Website. Spiced with extensive substantial 'Structured data markup'**

Load the Businesshour module on any page to a position, an article or in the sidebar.

<!--
* [Businesshour Demonstration](https://webmanagement.berlin/jmodule-businesshour)
* [Businesshour Screenshots](https://webmanagement.berlin/jmodule-businesshour)
-->

> Last Proof / Viewed by Human: Installation & Used with Joomla! 3.9.25 [20210504]

